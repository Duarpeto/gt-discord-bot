# Prefix used for bot commands
BOT_PREFIX = ("b?", "b!")

# What plugins to load from the `plugins` directory
COGS = (
    'meme',
    'notifications',
)

# The number of messages discord.py stores in memory
MAX_MESSAGES = 5000

# Discord Token for bot authentication
TOKEN = ''
