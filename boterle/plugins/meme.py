import json
from discord.ext import commands
from requests_futures.sessions import FuturesSession

from .base import BasePlugin

# All plugins should inherit form BasePlugin
class MeMePlugin(BasePlugin):
    # If you want to use sub commands you must use this decorator to create a group
    @commands.group(pass_context=True,
                    description="Get a dank meme: noris")
    async def meme(self, ctx):
        # Make sure to safely fail this command out!
        if ctx.invoked_subcommand is None:
            await self.bot.say('I can\'t meme about nothing!')

    # Use the previous method as a decorator for sub commands
    @meme.command(description="Chuck Noris Quote",
                  brief="Chuck Noris Quote")
    async def noris(self):
        # Opens an AYSNC request session
        session = FuturesSession()
        try:
            # Request and produce the http request
            response = session.get('https://api.chucknorris.io/jokes/random').result()
        except:
            await self.bot.say("The Chuck Noris service is down :(")
        # Deserialize the JSON response and return the result
        content = json.loads(response.content)
        await self.bot.say(content['value'])

# This function is used by bot.py to register the plugin
# This function takes the bots `self` variable so the plugin can reference the bot
def setup(bot):
    bot.add_cog(MeMePlugin(bot))

