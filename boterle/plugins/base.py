
"""
BasePlugin

Used as a plugin base for all Boterle Plugins
"""
class BasePlugin:
    def __init__(self, bot):
        self.bot = bot
