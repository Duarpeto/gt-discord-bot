from discord.utils import get
from discord.ext import commands

from utils import strip_id
from .base import BasePlugin


class NotificationPlugin(BasePlugin):
    @commands.group(pass_context=True,
                   description="Get added or removed to notification roles")
    async def notification(self, ctx):
        if ctx.invoked_subcommand is None:
            await self.bot.say('What do you want to do with notifications?')

    @notification.command(name='join',
                    description='Join a notification role',
                    brief='Join a notification role',
                    pass_context=True)
    async def join_role(self, context, role_name):
        if role_name:
            role = get(context.message.server.roles, id=await strip_id(role_name))
            author = context.message.author
            if "Notification" in str(role):
                await self.bot.add_roles(context.message.author, role)
                await self.bot.say("Added {} to the role {}".format(
                    author.mention,
                    role
                ))
            else:
                await self.bot.say("Foolish humans... Make sure you are picking a role from list_notification_roles")

    @notification.command(name='leave',
                    description='Leave a notification role',
                    brief='Leave a notification role',
                    pass_context=True)
    async def leave_role(self, context, role_name):
        if role_name:
            role = get(context.message.server.roles, id=await strip_id(role_name))
            author = context.message.author
            if "Notification" in str(role):
                await self.bot.remove_roles(context.message.author, role)
                await self.bot.say("Removed {} from the role {}".format(
                    author.mention,
                    role
                ))
            else:
                await self.bot.say("Foolish humans... Make sure you are picking a role from list_roles")

    @notification.command(name='list_roles',
                    descriptions='List all of the current joinable roles',
                    brief='List all of the current joinable roles',
                    pass_context=True)
    async def list_roles(self, context):
        roles = context.message.server.roles
        output = ""
        for role in roles:
            if "Notification" in str(role):
                output += str(role) + "\n"
        await self.bot.say(output)



def setup(bot):
    bot.add_cog(NotificationPlugin(bot))
