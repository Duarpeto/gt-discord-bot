import datetime
import os

from discord import Forbidden
from discord.ext.commands import Bot

from pluginbase import PluginBase
from settings.local_settings import BOT_PREFIX
from settings.local_settings import COGS
from settings.local_settings import MAX_MESSAGES


class Boterle(Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_time = datetime.datetime.now()
        # Gets the 'plugins' directory
        here = os.path.dirname(os.path.abspath(__file__))
        self.plugin_dir = os.path.join(here, 'plugins')
        # Creates a new place for pagebase to search for packages
        self.plugin_base = PluginBase(package='plugins')
        self.plugin_source = self.plugin_base.make_plugin_source(searchpath=[
            self.plugin_dir,
        ])
        # Loops through each of the items listed in local_settings.COG and loads the plugin
        for plugin in COGS:
            plugin_module = self.plugin_source.load_plugin(plugin)
            plugin_module.setup(self)

    async def dm_author(self, ctx, message):
        try:
            await self.send_message(ctx.message.author, message)

        except Forbidden:
            await self.send_message(message)


client = Boterle(messages=MAX_MESSAGES, command_prefix=BOT_PREFIX)

