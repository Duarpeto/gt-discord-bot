# GT Discord Bot

This bot is built to be a utility bot for the [Official Game Theory Discord](https://discord.gg/Vvp2u2r).

# Requirements

This project uses Pipenv to manage dependencies, if you dont already have it installed you can do so with the following command:

```
pip install pipenv
```

This project also uses Python 3.6 (tested 3.6.7), so you will need some version of 3.6 as well. Getting this will depend on your OS, but [python.org](https://www.python.org/downloads/) should help


# Installation

Once you have forked and cloned this repo to your machine, you can setup the dependencies like so:

```
pipenv install
```

Create a new `settings/local_settings.py` file with the following:

```
from .base import *

TOKEN = '<discord token>'

```

# Contributing

This bot uses discord.py so make sure you are familiar with that library. This bot also uses `pluginbasae` through the `boterle/plugins` directory. Plugins are described later in the `Plugin Basics` section.


This project has four main sections:

## bot.py 

This file contains the Boterle class that is the main class of our bot. The `__init__` method on `Boterle` also contains the plugin loading logic as well as a custom `dm_author` method that demonstrates a simple bot command.

## run.py

This file is used to start the bot. It will import from your custom `settings.local_settings` (next section) for a `TOKEN` constant you provide. The rest of the functions are just simple helper methods and starting the main event loop.

## settings

The `settings` folder contains one file `base.py` and expects you to provide one file `local_settings.py`. `base.py` should contain all settings for the bot that are not private (tokens) or personal (absolute file paths), that type of information should be placed in `settings/local_settings.py`.

By default `settings/local_settings.py` is ignored by git.

## plugins

Where all plugin files are stored.

# Plugin Basics

Plugins are handled through two different plugin systems used togeter. 

`discord.py` provides a great method off of `discord.ext.commands.Bot` called `add_cog`. The `add_cog` method allows us to add all of the methods/commands from one class onto our bot. This method is greatly prefered as opposed to having to define every method/command in one file in one class. Breaking up our code like this keeps everything much more readable.

`pluginbase` allows the us to define a list of directories (by default only `plugins` directory), loop over each file in each directory and load each module. By default the bot only loops over the plugins defined in `settings.local_settings.COGS`. Finally `pluginbase` runs each of the modules `setup` method to register te methods from the plugin with the bot.

A sample of a simple plugin can be seen in `plugins/memes.py` and `plugins/notification.py`.
